<?php


namespace DKDev\Api\Operation;


use DKDev\VisitStat;

class GetVisits implements ApiOperation
{
    public function run($request): array
    {
        if ($request['from'] === $request['to']) {
            $query = VisitStat::byDay($request['from']);
        } else {
            $query = VisitStat::byPeriod($request['from'], $request['to']);
        }
        // dump($query);

        $rsData = $query->where('period', $request['period'])
                        ->orderBy('date')
                        ->orderBy('period_num')
                        ->get();

        return $rsData->toArray();
    }
}
<?php

namespace DKDev\Log;

use Symfony\Component\HttpFoundation\Request;

interface LoggerInterface
{
    public function add(Request $request);
}
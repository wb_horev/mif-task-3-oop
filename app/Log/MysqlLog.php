<?php


namespace DKDev\Log;


use DKDev\App;
use Illuminate\Support\Carbon;
use Monolog\Logger;
use MySQLHandler\MySQLHandler;
use Symfony\Component\HttpFoundation\Request;

class MysqlLog implements LoggerInterface
{
    /* @var Logger */
    protected static $log;

    public function add(Request $request)
    {
        if (!static::$log) {
            $mySQLHandler = new MySQLHandler(
                App::Capsule()->getConnection()->getPdo(),
                "api_log_request",
                ['ip', 'request'],
                Logger::INFO);

            static::$log = new Logger('MysqlLog');
            static::$log->pushHandler($mySQLHandler);
        }

        static::$log->info('API request:', [
            'time'    => Carbon::now(),
            'ip'      => $request->server->get('REMOTE_ADDR'),
            'request' => json_encode($request->query->all()),
        ]);
    }
}
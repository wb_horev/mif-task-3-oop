<?php


namespace DKDev\Operation;


use DKDev\Visit;
use DKDev\VisitStat;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

class ParseVisitorStats
{
    protected static $step = 10000;
    protected static $DAY_SECONDS = 86400;
    public static $defaultPeriod = '1d';
    public static $arPeriod = [
        '1h' => 3600,
        '1d' => 86400,
    ];

    public function make($request)
    {
        // $this->makeOnlineStatistics($request['date'], $request['period']);
        // die;
        $request['period'] = $this->validatePeriod($request['period']);

        // чистка статистики если запросили генерацию сначала (тут только для удобства тестирования задания)
        if ((int)$request['lastId'] === 0) {
            VisitStat::query()->where('period', $request['period'])->delete();
        }

        $fromDate = Carbon::parse($request['from']);
        $toDate = Carbon::parse($request['to']);
        // dump($colVisit);
        // die;

        $nRecordCount = 0;
        $arLastId = [];

        $dateParse = $fromDate->copy();
        while ($dateParse <= $toDate) {
            $arPeriods = $this->makePeriods($request['period'], $dateParse);

            $colVisit = Visit::byDay($dateParse)
                             ->where('id', '>', $request['lastId'])
                             ->take(static::$step)
                             ->get();

            // группируем по IP (уникальность)
            $colFiltered = $colVisit->groupBy('ip');

            $arStatistic = $this->makeStatistics($arPeriods, $colFiltered);

            $this->updateStatistic($arStatistic, $dateParse, $request['period']);

            $dateParse->addDay();

            $nRecordCount += $colVisit->count();
            $arLastId[] = $colVisit->last()->id;
        }

        return [
            'lastId'     => max($arLastId),
            'count'      => $nRecordCount,
            'isFinished' => ($nRecordCount < static::$step),
        ];
    }

    /**
     * @param        $period
     * @param Carbon $date
     *
     * @return array
     */
    private function makePeriods($period, $date)
    {
        $seconds = static::$arPeriod[$period];

        if (!$seconds) {
            $seconds = static::$arPeriod[static::$defaultPeriod];
        }

        $count = static::$DAY_SECONDS / $seconds;

        $arRes = [];
        for ($i = 0; $i < $count; $i++) {
            $arRes[] = $date->copy()->addSeconds($i * $seconds);
        }

        return $arRes;
    }

    /**
     * @param Carbon $time
     * @param array  $arPeriods
     *
     * @return int|mixed|string
     */
    private function findPeriod(Carbon $time, array $arPeriods)
    {
        static $cachePeriods = [];

        $searchTime = $time->getTimestamp();

        if ($cachePeriods[$searchTime]) {
            return $cachePeriods[$searchTime];
        }

        /* @var $period Carbon */
        $prev = current($arPeriods);
        foreach ($arPeriods as $key => $period) {
            if (
                $prev->getTimestamp() <= $searchTime
                && $searchTime < $period->getTimestamp()
            ) {
                $cachePeriods[$searchTime] = $key - 1;

                return $cachePeriods[$searchTime];
            }
            $prev = $period;
        }

        if (!$cachePeriods[$searchTime]) {
            $cachePeriods[$searchTime] = end(array_keys($arPeriods));
        }

        return $cachePeriods[$searchTime];
    }

    /**
     * @param array $arPeriods
     * @param       $colVisit
     *
     * @return array
     */
    private function makeStatistics(array $arPeriods, $colVisit)
    {
        $arStatistic = [];
        /* @var Collection $visitGroup */
        foreach ($colVisit as $visitGroup) {
            $visit = $visitGroup->last();
            $curPeriod = $this->findPeriod($visit->datetime, $arPeriods);
            $arStatistic[$curPeriod] += 1;
        }

        return $arStatistic;
    }

    /**
     * @param array  $arStatistic
     * @param Carbon $date
     * @param        $period
     *
     * @return bool
     */
    private function updateStatistic(array $arStatistic, $date, $period)
    {
        $rsUpdate = VisitStat::query()
                             ->where('date', $date)
                             ->where('period', $period)
                             ->whereIn('period_num', array_keys($arStatistic))
                             ->get();
        // dump($rsUpdate);
        // die;

        // обновляем сначала существующие записи
        // todo можно обновить все через транзакцию за 1 запрос, но пока не стал делать
        if ($rsUpdate->count()) {
            /* @var VisitStat $updatingVisit */
            foreach ($rsUpdate as $updatingVisit) {
                $nVisits = $arStatistic[$updatingVisit->period_num];
                $updatingVisit->visits += $nVisits;
                $updatingVisit->save();

                // чистим массив на запись
                unset($arStatistic[$updatingVisit->period_num]);
            }
        }

        // добавляем новые записи
        if (count($arStatistic)) {
            return $this->insertStatistic($arStatistic, $date, $period);
        }

        return true;
    }

    /**
     * @param array  $arStatistic
     * @param Carbon $date
     * @param        $period
     *
     * @return bool
     */
    private function insertStatistic(array $arStatistic, $date, $period)
    {
        $arInsert = [];
        foreach ($arStatistic as $period_num => $nVisits) {
            $arInsert[] = [
                'date'       => $date,
                'period'     => $period,
                'period_num' => $period_num,
                'visits'     => $nVisits,
            ];
        }

        return VisitStat::query()->insert($arInsert);
    }

    /**
     * @param $date
     * @param $period
     */
    private function makeOnlineStatistics($date, $period)
    {
        $rsStats = VisitorStats::query()
                               ->where('date', Carbon::parse($date))
                               ->where('period', $period)
                               ->get();

        $prevOnline = 0;
        foreach ($rsStats as $rsPeriod) {
            $nDelta = $rsPeriod->in - $rsPeriod->out;
            $rsPeriod->online += $prevOnline + $nDelta;

            // fix для демо данных, из-за рандома может быть минус онлайн
            $rsPeriod->online = max(0, $rsPeriod->online);

            $prevOnline = $rsPeriod->online;

            $rsPeriod->save();
        }
    }

    /**
     * @param $period
     *
     * @return string
     */
    private function validatePeriod($period)
    {
        $seconds = static::$arPeriod[$period];

        if (!$seconds) {
            $period = static::$defaultPeriod;
        }

        return $period;
    }
}

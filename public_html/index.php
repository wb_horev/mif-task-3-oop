<?php

use DKDev\App;
use DKDev\Visit;
use Illuminate\Support\Carbon;

require_once '../init/init.php';

$request = App::Request();
$ar = array_merge($request->query->all(), $request->request->all());


// dump($ar);
// dump($request->server->get('REMOTE_ADDR'));

$visit = [
    'datetime' => Carbon::now(),
    'ip'       => $request->server->get('REMOTE_ADDR'),
];

$ok = Visit::saveNew($visit);

if ($ok) {
    dump('saved:');
    dump($visit);
}

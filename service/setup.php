<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

require_once '../init/init.php';

// Миграция для демо
$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => DB_HOST,
    'database'  => null,
    'username'  => DB_USER,
    'password'  => DB_PASS,
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

// create DB
$dbName = DB_NAME;
$capsule->getConnection()->getPdo()->exec("CREATE DATABASE IF NOT EXISTS `{$dbName}`");
$capsule->getConnection()->getPdo()->exec("USE `{$dbName}`");
$capsule->getConnection()->setDatabaseName($dbName);

// create table
if (!$capsule->schema()->hasTable('visits')) {
    $capsule->schema()->create('visits', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->charset = 'utf8';
        $table->collation = 'utf8_unicode_ci';

        $table->bigIncrements('id');
        $table->dateTime('datetime');
        $table->string('ip');
    });
}

// create table
if (!$capsule->schema()->hasTable('visit_stats')) {
    $capsule->schema()->create('visit_stats', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->charset = 'utf8';
        $table->collation = 'utf8_unicode_ci';

        $table->bigIncrements('id');
        $table->date('date');
        $table->string('period');
        $table->smallInteger('period_num');
        $table->integer('visits');
    });
}

// create table
if (!$capsule->schema()->hasTable('api_log_request')) {
    $capsule->schema()->create('api_log_request', function (Blueprint $table) {
        $table->engine = 'InnoDB';
        $table->charset = 'utf8';
        $table->collation = 'utf8_unicode_ci';

        $table->bigIncrements('id');
        $table->string('channel');
        $table->string('level');
        $table->string('message');
        $table->dateTime('time');
        $table->string('ip');
        $table->text('request');
    });
}

echo 'OK';
